+++
date = 2018-01-01
# lastmod = 2017-09-03
draft = false
tags = ["Mexique", "Outils"]
title = "Outils"
summary = """ Outils mis en oeuvre au cours de ce projet."""
+++

# Introduction
Au début de ce projet, nous étions libre du choix des outils, formés à 
[SPAD](https://www.coheris.com/produits/analytics/logiciel-data-mining/) ou 
[SAS](https://www.sas.com/fr_fr/home.html), nous aurions très bien pu nous 
orienter vers ces outils.  Mais nous avons aussi été formés au language 
[R](https://fr.wikipedia.org/wiki/R_(langage)) qui a l'avantage d'être plus 
versatile que ces concurrents en ne se limitant pas à l'analyse de données 
(par exemple en permettant la cartographie des résultats).

L'écosystème **R** a aussi permis de contrôler la chaîne éditoriale au sein d'un 
même logiciel. Ainsi le rapport a été rédigé grâce à *liftr*, la présentation 
utilisée pour la soutenance grâce à *Xaringan* et le présent site grâce à 
*blogdown*. Tous sont des packages R qui permettent d'étendre les capacités du 
language au-delà de la simple analyse de données.


# Outils analytiques
Nous avons recouru aux outils analytiques dits "de l'école française" qui nous 
ont été enseignés au cours de l'année. Tous ne se prêtent pas à l'exploitation des
données utilisés mais ont été envisagés.

## Analyse en Composantes Principales
Cet outil d'analyse se prête moins à l'analyse de données socio-démographiques 
telles que celles dont nous disposions.

## Analyse Factorielle des Correspondances
## Classification Ascendante Hiérarchique

# Outils logiciels
Comme dit précédemment, nous avons recouru principalement au language R. En effet,
il est très versatile et nous permet en autre de réaliser un maximum de choses 
avec un outil unique. De plus, nous avons une réelle appétance pour ce language et souhaitions 
le découvrir un peu plus.

## R

R est un language de programmation libre et gratuit, il présente l'avantage de 
pouvoir être installé sur tout type de système d'exploitation sans avoir à recourir 
à une machine virtuelle ou à de l'émulation. Ainsi, pour les traitements de 
gros volumes de données, l'accès direct au processeur et à la mémoire vive permet 
d'éviter certaines limitations et de gagner en performance.

### Composants analytiques
### Ecosystème R
Au delà des composant analytiques, nous nous sommes servis de R pour la partie éditoriale
de ce projet et ceci grâce à plusieurs paquets. Beaucoup de ces paquets ont recours 
au [*Rmarkdown*](http://rmarkdown.rstudio.com/), un langage de balisage simple.

Nous souhaitons remercier les auteurs de ces paquets. 

#### liftr

Le paquet [*liftr*](https://liftr.me/) permet la création, à partir d'un fichier en *Rmarkdown*, d'un document
pdf comprenant le code ainsi que les figures générées.  Outre cette possibilité, il 
dispose des qualités héritées du langage [LaTeX](https://fr.wikipedia.org/wiki/LaTeX) : 

- numérotation automatique des sections
- table des matière automatique
- suivi de la bibliographie

![Flux de travail avec *liftr*](../../img/liftr.png)

Ce paquet a été créé par [*Nan Xiao*](https://nanx.me/).

#### blogdown

Le paquet [*blogdown*](https://bookdown.org/yihui/blogdown/) permet la création de sites web statiques (à oposer aux sites dynamiques qui s'adaptent à l'utilisateur). Basé sur le générateur de site [hugo](https://gohugo.io/), il reprend sa syntaxe simple en y ajoutant que la souplesse du *Rmarkdown* et la possibilité d'exécuter du code *R*.

Semblant correspondre à nos besoins de publication dans le cadre de ce projet, il nous a éviter d'avoir recours à une solution de type [Wordpress](https://fr.wordpress.com/) qui nécessite d'avoir recours à un écosystème plus complexe (*PHP* et/ou *Javascript*, bases de données type *MySQL*, etc)

Ce paquet a été créé par [*Yihui Xie*](https://yihui.name/).


#### Xaringan
Ce paquet a aussi été créé par [*Yihui Xie*](https://yihui.name/), il permet à l'aide d'un fichier en Rmarkdown de compiler une présentation utilisant *Javascript*, qu'il est possible de lire sur tous les navigateurs modernes.

# Versionnement et hébergement du projet et du site

Le versionnement du projet a été réalisé à l'aide de [git](https://git-scm.com/). Cela permet un réel travail collaboratif, sans avoir à s'échanger des fichiers par mail.
Le projet est divisé en deux dépôts git:

- celui contenant le [rapport ainsi que la présentation](https://framagit.org/m2_projet_mexique/rapport) utilisée lors de la soutenance;
- celui hébergeant et diffusant le [site internet](https://framagit.org/m2_projet_mexique/website).

Ces dépôts git sont hébergés sur [**Framagit**](https://framagit.org/), une instance [*Gitlab*](https://about.gitlab.com/) mise à disposition gratuitement par l'association [Framasoft](https://framasoft.org/). Le site même que vous êtes en train de consulter est sur [**Framagit**](https://framagit.org/) (en utilisant avantageusement les [Gitlab Pages](https://framablog.org/2017/03/20/les-gitlab-pages-debarquent-dans-framagit/))

Nous tenons à les remercier chaleureusement.


<img src="../../img/Logo_Framasoft.png" alt="Logo de l'association Framasoft" style="width: 300px;"/>


# Python

La langage de programmation [**Python**](https://www.python.org/) a été utilisé pour traiter et filtrer les données. En effet, les données de l'année 2015 représentaient près de 7 Gigaoctets, divisés en 64 fichiers *csv*. Malheureusement, nous n'avons pas su mettre en oeuvre **R** pour traiter toutes ces données, et cela bien qu'ayant essayé divers paquets tels de [fread](https://www.rdocumentation.org/packages/data.table/versions/1.10.4-2/topics/fread) ou [ffbase](https://cran.r-project.org/web/packages/ffbase/index.html). Nous avons donc recouru à **Python** et notamment à la librairie [Pandas](http://pandas.pydata.org/). Celle-ci a permis de manipuler les données, de les fusionner et, une fois le sujet précisément déterminé, de les filtrer pour ne garder que les lignes et les colonnes qui nous intéressaient. Nous sommes ainsi passé d'un fichier *csv* de près de 7 Go (22 millions de lignes, 186 variables) à un condensé de 300 Mo (près de 400 000 lignes, environ 50 variables).

A l'usage, pour le traitement de gros volumes de données tel que c'était le cas, **Python** nous a semblé plus souple d'utilisation et moins exigeant en ressources.

# Conclusion

Nous avons pu manipuler un grand nombre d'outils, apportant chacun des solutions plus ou moins complexes à mettre en oeuvre. Le fait de travailler un maximum avec **R** a créé une contrainte plutôt intéressante et a permis de s'investir davantage dans la compréhension de ce langage.