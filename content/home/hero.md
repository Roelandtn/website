+++
# Hero widget.
widget = "hero"
active = true
date = 2017-01-01

title = "Division sociale de l'espace résidentiel dans la Zone métropolitaine de Mexico: diagnostic et typologies des quartiers"

# Order that this section will appear in.
weight = 3

# Overlay a color or image (optional).
#   Deactivate an option by commenting out the line, prefixing it with `#`.
[header]
  overlay_color = "#666"  # An HTML color value.
  overlay_img = "headers/Mapa_de_las_lagunas_rios_y_lugares_que_circundan_a_Mexico_1280_400.jpg"  # Image path relative to your `static/img/` folder.
  overlay_filter = 0.5  # Darken the image. Value in range 0-1.

# Call to action button (optional).
#   Activate the button by specifying a URL and button label below.
#   Deactivate by commenting out parameters, prefixing lines with `#`.
# [cta]
#   url = "./post/getting-started/"
#   label = '<i class="fa fa-download"></i> Install Now'
+++

Ce projet a pour objet la réalisation d'une étude de la division sociale de l'espace résidentiel de la Zone métropolitaine de Mexico à l'aide d'outils d'analyse de données. Les données sont issues de l'INEGI (Institut national des statistiques mexicain).
