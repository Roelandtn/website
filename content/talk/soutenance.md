+++
date = 2018-01-01T13:26:00  # Schedule page publish date.

title = "Soutenance"
time_start = 2018-01-11T09:00:00
time_end = 2018-01-11T12:00:00
abstract = ""
abstract_short = ""
event = "Soutenances des projets transversaux"
event_url = ""
location = "Université Paris8"

# Is this a selected talk? (true/false)
selected = false

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter the filename (excluding '.md') of your project file in `content/project/`.
projects = ["Rapport","Website"]

# Links (optional).
url_pdf = ""
url_slides = "https://framagit.org/m2_projet_mexique/rapport/tree/master/docs/presentation"
url_video = ""
url_code = ""

# Does the content use math formatting?
math = true

# Does the content use source code highlighting?
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = "headers/Mapa_de_las_lagunas_rios_y_lugares_que_circundan_a_Mexico_1280.jpg"
caption = "[Creative Commons](https://commons.wikimedia.org/wiki/File:%22Mapa_de_las_lagunas_rios_y_lugares_que_circundan_a_Mexico...%22_(22341819185).jpg)"

+++

Embed your slides or video here using [shortcodes](https://sourcethemes.com/academic/post/writing-markdown-latex/). Further details can easily be added using *Markdown* and $\rm \LaTeX$ math code.
